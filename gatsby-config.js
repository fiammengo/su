/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
  /* Your site config here */
  plugins: [
    `gatsby-plugin-material-ui`,
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [
          `Roboto`, 
          'Architects Daughter',
          'Kalam\:300,400',
          'Dancing Script',
          `source sans pro\:300,400,400i,700`, // you can also specify font weights and styles
          'Marck Script',
          'Satisfy',
        ],
        display: 'swap'
      }
    },
    {
      resolve: `gatsby-plugin-emotion`,
      options: {
        // Accepts all options defined by `babel-plugin-emotion` plugin.
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `SusanaCanel`,
        short_name: `SusanaCanel`,
        start_url: `/`,
        background_color: `#f7f0eb`,
        theme_color: `#a2466c`,
        display: `standalone`,
        icon: `src/img/susanacanel.jpg`,
      },
    },
  ],
}

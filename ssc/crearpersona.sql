create table if not exists persona (
  id
    int
    not null 
    auto_increment
    primary key
    comment 'Un número autogenerado. Cada persona tiene, y es identificada por, su id'
    ,
  sobrenombre 
    varchar( 16)  
    not null
    comment 'El nick o sobrenombre de la persona, por ahora también va acá el nombre, si lo puso'
    ,
  correo       
    varchar(256)  
    not null
    comment 'La dirección de correo electrónico de la persona'

);

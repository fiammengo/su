-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-06-2020 a las 23:23:15
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sitiosusanacanel`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario`
--
USE sitiosusanacanel

CREATE TABLE `comentario` (
  `id_comentario` int(11) NOT NULL,
  `url_articulo` varchar(256) COLLATE utf8_spanish2_ci NOT NULL,
  `id_comentario_origen` int(11) NOT NULL,
  `fecha_recibido` int(11) NOT NULL,
  `id_carpeta` int(11) NOT NULL,
  `texto` varchar(4096) COLLATE utf8_spanish2_ci NOT NULL,
  `id_usuario` varchar(16) COLLATE utf8_spanish2_ci NOT NULL,
  `asunto` varchar(256) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `comentario`
--

INSERT INTO `comentario` (`id_comentario`, `url_articulo`, `id_comentario_origen`, `fecha_recibido`, `id_carpeta`, `texto`, `id_usuario`, `asunto`) VALUES
(157, 'http://localhost/blog/tips-licencia.php', 0, 1586576154, 2, 'Caminaba por esa encantadora callecita', '2', 'Comentario'),
(158, 'http://localhost/blog/tips-licencia.php', 0, 1586576930, 1, 'TIEMPO 5', '2', 'Comentario'),
(159, 'http://localhost/blog/tips-licencia.php', 0, 1586576931, 1, 'TIEMPO 5', '2', 'Comentario'),
(160, 'http://localhost/blog/001-anecdotas-ovejas.php', 0, 1586576977, 2, 'HOLA 6', '2', 'Comentario'),
(161, 'http://localhost/blog/001-anecdotas-ovejas.php', 0, 1586577252, 1, 'hola 7', '2', 'Comentario'),
(162, 'http://localhost/blog/001-anecdotas-ovejas.php', 0, 1586577253, 1, 'hola 7', '2', 'Comentario'),
(163, 'http://localhost/blog/tips-licencia.php', 0, 1586578543, 2, 'hola 8', '2', 'Comentario'),
(164, 'http://localhost/blog/tips-licencia.php', 0, 1586578564, 2, 'Se dirigían al mismo parque que yo, ja, ja, pero ellas iban mucho más rápido, me dejaron atrás…Las corrí desconsolada. Aquí ves a las encantadoras ovejitas.', '2', 'Comentario'),
(165, 'http://localhost/blog/tips-licencia.php', 0, 1586578586, 2, 'Caminaba por esa encantadora callecita cerca de la via Appia Antica, sin vereda, tratando de que los autos no me llevaran por delante, cuando sentí un extraño ruido a mis espaldas, pero verdaderamente extraño, nunca antes escuchado por mí. Difícil de explicar ', '2', 'Comentario');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `comentario`
--
ALTER TABLE `comentario`
  ADD PRIMARY KEY (`id_comentario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `comentario`
--
ALTER TABLE `comentario`
  MODIFY `id_comentario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=166;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

insert into persona (sobrenombre, correo) values ('sys', 'sys@susanacanel.com');
insert into persona (sobrenombre, correo) values ('luke', 'luke@tatooine.com');
insert into persona (sobrenombre, correo) values ('han', 'han@tatooine.com');
insert into persona (sobrenombre, correo) values ('leia', 'leia@tatooine.com');
insert into persona (sobrenombre, correo) values ('obi', 'obi@tatooine.com');
insert into persona (sobrenombre, correo) values ('ren', 'ren@tatooine.com');

insert into mensaje (autor,texto, tipo, estado, referencia, articulo)
  values (1,'root messaje', 'asesoramiento','publicado',null, 'http://susanacanel.com');
insert into mensaje (autor,texto, tipo, estado, referencia, articulo)
  values (4,'Help me, Obi Wan Kenobi, you are my only hope', 'asesoramiento','pendiente',null, 'http://imperialcruiser.com');
insert into mensaje (autor,texto, tipo, estado, referencia, articulo)
  values (5,'May the force be with you, trust in your instincts', 'respuesta','pendiente',2, 'http://imperialcruiser.com');


create table if not exists mensaje (

  id            
    int 
    not null 
    auto_increment
    primary key
    comment 'Un número autogenerado. Cada mensaje tiene, y es identificado por, su id'
    ,
  autor
    int 
    not null
    comment 'El id de la persona autora del comentario (debe existir en la tabla persona)'
    ,
  estampado     
    timestamp
    comment 'Año, mes, día, hora, minuto y segundo del momento en que es creado el mensaje'
    ,
  texto         
    text
    comment 'El texto del mensaje'
    ,
  tipo          
    enum ('asesoramiento','contacto','subscripcion','comentario','respuesta') 
    not null
    comment 'El tipo de mensaje (relacionado con la página que lo originó, pero no son lo mismo)'
    ,
  estado
    enum('pendiente','publicado','spam','borrado')
    default('pendiente')
    not null
    comment 'El estado del mensaje, solo los "publicado"s aparecen en la pagina'
    ,
  referencia    
    int
    comment 'Si este mensaje es una respuesta, acá viene el id del mensaje al cual este responde'
    ,
  articulo
    varchar(256)  
    not null
    comment 'La url de la página en que fué creado el mensaje. Esta url actúa como la indentificación del "topic"'
    ,
  index par_ind (autor),
  constraint fk_persona foreign key (autor)
    references persona(id)
    on delete cascade
    on update cascade


);

create table if not exists posts (

  id            
    int 
    not null 
    auto_increment
    primary key
    comment 'Un número autogenerado. Cada mensaje tiene, y es identificado por, su id'
    ,
  title
    varchar(256),
  content
    varchar(256),
  created
    varchar(256)

);

import React from "react"
import Grid from "@material-ui/core/Grid"
import { makeStyles } from "@material-ui/core/styles"
import FormControl from "@material-ui/core/FormControl"
import InputLabel from "@material-ui/core/InputLabel"
import OutlinedInput from "@material-ui/core/OutlinedInput"
import FormHelperText from "@material-ui/core/FormHelperText"
import Button from "@material-ui/core/Button"
import Marco from "../components/marco"
import Minipagina from "../components/minipagina"
import { Container } from "@material-ui/core"
import Titular from "../components/titular"
import EspaciadorVertical from "../components/espaciador-vertical"
const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
  },
  margin: {
    margin: theme.spacing(1),
    marginLeft: theme.spacing(0),
  },
  withoutLabel: {
    marginTop: theme.spacing(3),
  },
  textField: {
    width: "25ch",
  },
}))

export default function Asesoramiento() {
  const classes = useStyles()
  const [values, setValues] = React.useState({
    sobrenombre: "",
    correo: "",
    asunto: "",
    mensaje: "",
  })

  /*
  const [valids, setValids] = React.useState({
    sobrenombre: true,
    correo: true,
    asunto: true,
    mensaje: true,
  })
  */

  const handleChange = prop => event => {
    setValues({
      ...values,
      [prop]: event.target.value,
    })
  }

  return (
    <Marco>
      <Container>
        <EspaciadorVertical altura={48} />
        <Grid container spacing={2} justify="center" alignItems="flex-start">
          <Minipagina>
            <EspaciadorVertical altura={36} />

            <Titular texto="Planifico tu viaje" />
            <div style={{ paddingRight: "2rem" }}>
              <p>
                ¿Te gustaría conocer Roma de manera no tradicional? ¿Caminarla y descubrir lugares
                encantadores? ¡Pero te falta tiempo para organizar tu viaje!
              </p>
              <p>
                Y sí, buscar toda la información lleva mucho tiempo. Y qué decir de planificar tus recorridos.
              </p>
              <p>
                ¡Ahí estoy yo para ayudarte!
                <span aria-label="smile" role="img">
                  🙂
                </span>
                Mi trabajo consiste en proponerte recorridos personalizados para hacer caminado (eventualmente
                en bici). Me decís tus gustos, tus días disponibles, tus necesidades y … ¡Listo! Recibís mis
                propuestas junto con información que te pueda resultar útil elaborada de manera novedosa.
              </p>
              <p>
                Contáctame llenando el siguiente formulario y coordinamos una charla por whatsapp, skype,
                teléfono o café mediante. El asesoramiento lo envío por email. No necesitás moverte de tu
                casa.
              </p>
            </div>
          </Minipagina>
          <Minipagina palida>
            <EspaciadorVertical altura={36} />
            <Titular texto="Contáctame" color="primary" />
            <div name="formulario">
              <FormControl fullWidth className={classes.margin} variant="outlined">
                <InputLabel htmlFor="outlined-adornment-nombre">Tu nombre o nickname</InputLabel>
                <OutlinedInput
                  id="outlined-adornment-nombre"
                  value={values.nombre}
                  onChange={handleChange("nombre")}
                  labelWidth={128}
                />
                <FormHelperText required id="component-helper-text"></FormHelperText>
              </FormControl>
              <FormControl fullWidth className={classes.margin} variant="outlined">
                <InputLabel htmlFor="outlined-adornment-correo">
                  Tu dirección de correo electrónico
                </InputLabel>
                <OutlinedInput
                  id="outlined-adornment-correo"
                  error
                  value={values.correo}
                  onChange={handleChange("correo")}
                  labelWidth={186}
                />
                <FormHelperText required error id="component-helper-text">
                  Eso de ahi arriba no parece un correo electronico
                </FormHelperText>
              </FormControl>
              <FormControl fullWidth className={classes.margin} variant="outlined">
                <InputLabel htmlFor="outlined-adornment-amount">El motivo del contacto</InputLabel>
                <OutlinedInput
                  id="outlined-adornment-asunto"
                  value={values.asunto}
                  onChange={handleChange("asunto")}
                  labelWidth={124}
                />
                <FormHelperText id="component-helper-text"> </FormHelperText>
              </FormControl>
              <FormControl fullWidth className={classes.margin} variant="outlined">
                <InputLabel htmlFor="outlined-adornment-amount">Tu mensaje</InputLabel>
                <OutlinedInput
                  id="outlined-adornment-mensaje"
                  value={values.mensaje}
                  onChange={handleChange("mensaje")}
                  labelWidth={66}
                  multiline
                  rows={6}
                />
                <FormHelperText id="component-helper-text"> </FormHelperText>
              </FormControl>
              <div style={{ position: "relative", height: 24 }}>
                <Button
                  variant="outlined"
                  color="primary"
                  href="#outlined-buttons"
                  style={{ position: "absolute", right: 0 }}
                >
                  ENVIAR
                </Button>
              </div>
            </div>
            <EspaciadorVertical altura={24} />
          </Minipagina>
        </Grid>
        <EspaciadorVertical altura={48} />
      </Container>
    </Marco>
  )
}

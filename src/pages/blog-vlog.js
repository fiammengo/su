import React from "react"
import Marco from "../components/marco"
import imgblogasiciliacefalu01CefaluPlayaybotesjpg from "../img/blog/a-sicilia/cefalu/01-Cefalu-Playa-y-botes.jpg"
import imgblogaovejasappia01viadiceciliametellajpg from "../img/blog/a-ovejas-appia/01-via-di-cecilia-metella.jpg"
import imgblogtleonardoexpress1leonardoexpress1jpg from "../img/blog/t-leonardo-express/1-leonardo-express-1.jpg"
import imgblogtconvalidareconvalida3jpg from "../img/blog/t-convalidare/convalida3.jpg"
import imgblogasiciliacasamontalbano01casamontalbano1jpg from "../img/blog/a-sicilia/casa-montalbano/01-casa-montalbano1.jpg"
import imgblogtbalancita1balancitajpg from "../img/blog/t-balancita/1-balancita.jpg"
import imgblogtcelularcelularjpg from "../img/blog/t-celular/celular.jpg"
import imgblogtvalijas1valijaconcinta4ruedasjpg from "../img/blog/t-valijas/1-valija-con-cinta-4-ruedas.jpg"
import imgblogtaviones1enezeizajpg from "../img/blog/t-aviones/1-en-ezeiza.jpg"
import imgblogtautoViajandoenautojpg from "../img/blog/t-auto/Viajando-en-auto.jpg"
import imgblogtvalijas2enfiumicinoconvalijasjpg from "../img/blog/t-valijas/2-en-fiumicino-con-valijas.jpg"
import imgblogtlicenciaLicenciadeconducirinternacionaljpg from "../img/blog/t-licencia/Licencia-de-conducir-internacional.jpg"
import imgblogrbienvenidos1encastelsantangelojpg from "../img/blog/r-bienvenidos/1-en-castel-sant'angelo.jpg"
import { css } from "@emotion/core"
import styled from "@emotion/styled"

const TituloArticulos = styled.h3`
  color: #16455a;
  width: 80%;
  margin-left: auto;
  margin-right: auto;
  text-align: center;
  font-size: 3.5rem;
  font-family: "Satisfy";
  font-weight: 300;
`

const SideBarIzquierdo = styled.div`
  flex: 2.1;
  @media (max-width: 900px) {
    flex: 1.5;
  }
  @media (max-width: 500px) {
    flex: 1;
  }   
`
const SideBarDerecho = styled.div`
  flex: 2.1;
  @media (max-width: 900px) {
    flex: 1.5;
  }
  @media (max-width: 500px) {
    flex: 1;
  }   
`
const ContenedorBlog = styled.div` 
    flex: 3.9;
    @media (max-width: 900px) {
      flex: 9;
    }
    @media (max-width: 500px) {
      flex: 8;
    }   
    flex-direction: column;
    background-color: white;
    margin-top: 2rem;
    margin-bottom: 2rem;
    padding-bottom: 1rem;
    padding-right: 2.5rem;
    padding-left: 2.5rem;
    padding-top: 1.5rem;
 `

const LayoutEntradas = styled.div`
  display:flex;
 `
const Entradas = ({ children }) => (
  <LayoutEntradas>
    <SideBarIzquierdo/>
    <ContenedorBlog>
      {children}
    </ContenedorBlog>
    <SideBarDerecho/>
  </LayoutEntradas>
)

const EntradaBlog = styled.div`
  display: flex;
  flex: 
  flex-direction: row;
  flex-wrap: wrap;
  margin-bottom: 1rem;
  margin-right: 1rem;
  margin-bottom: 1.7rem;
  align-items: flex-start;
  background-color: white;
  &:first-of-type {
    padding-top: 1rem;
  }
  @media (max-width: 768px) {
    flex-direction: column;
    margin-bottom: 1rem;
    border-bottom: 1px solid #d0d0d0;
    margin-right: 1rem;
  }
`

const fotomin = css`
  object-fit: contain;
  width: 120px;
  @media (max-width: 768px) {
    width: 50%;
  }
`
const miniatura = css`
  flex: 1;
  padding-top: 3px;
  @media (max-width: 900px) {
    padding-left: 1rem;
  }
`

const Miniatura = ({src}) =>
  <div css={miniatura}>
    <img css={fotomin} alt="" src={src} />
  </div>

const DescripcionIndice = styled.div`
  flex: 7;
  font-size: 12px;
  background-color: white;
  color: black;
  padding-left: 2rem;
  padding-right: 1rem;
  & a {
    color: red;
    text-decoration: none;
  }
  @media (max-width: 900px) {
    padding-right: 2rem;
  }
  @media (max-width: 768px) {
    padding-left: 1rem;
    flex: 1;
  }
  @media (max-width: 500px) {
    flex: 1;
  }  
`
const TextoDescripcion = styled.div`
  margin-bottom: 6px;
`
const Entrada = ({ src, tit, tags, fecha, children }) => 
  <EntradaBlog>
    <Miniatura src={src} />
    <DescripcionIndice>
      <TituloEntrada>{tit}</TituloEntrada>
      <TextoDescripcion>{children}</TextoDescripcion>
      <TagsYFecha tags={tags} fecha={fecha} />
    </DescripcionIndice>
  </EntradaBlog>

const TituloEntrada = styled.h3`
  margin-top: 0;
  font-weight: 600;
`
//#region Tags y fecha
const Tags = styled.div`
  width: 60%;
`
const Fecha = styled.div`
  width: 40%;
  text-align: right;
  color: #808080;
  font-family: Satisfy;
  padding-right: 8;
`
const tyf = css`
  display: flex;
`
const TagsYFecha = ({ tags, fecha }) => (
  <div css={tyf}>
    <Tags>{tags}</Tags>
    <Fecha>{fecha}</Fecha>
  </div>
)
//#endregion 

const BlogVlog = () => 
  <Marco>
    <Entradas>
      <TituloArticulos>Aquí tienes mis artículos</TituloArticulos>
      <Entrada
        cefalu
        src={imgblogasiciliacefalu01CefaluPlayaybotesjpg}
        tit="Descubriendo Cefalù, Sicilia."
        tags="#relatos #anécdotas #sicilia"
        fecha="7 de marzo de 2014"
      >
        Y por suerte fui a Cefalù ¡Qué bello que es! Me alojaba en Palermo y
        decidí dedicar medio día para andar en tren a conocer Cefalù y
        volver a la noche. Como siempre me sucede, me resultó muy poco
        tiempo!
        <a href="/relatos-cefalu"> [Leer más...]</a>
      </Entrada>
      <Entrada
        ovejitas
        src={imgblogaovejasappia01viadiceciliametellajpg}
        tit="Via di Cecilia Metella. Parco Archeologico Via Appia Antica."
        tags="#anécdotas #appia-antica"
        fecha="7 de marzo de 2014"
      >
        Fui a caminar por la zona del parque arquelógico de la Via Appia
        Antica. Bajé del subte y comencé a caminar. Via di Cecilia Metella.
        Parco Archeologico Via Appia Antica. En mi caminata llegué a la Via
        di Cecilia Metella,
        <a href="/anecdotas-ovejas"> [Leer más...]</a>
      </Entrada>
      <Entrada
        fiumicino
        src={imgblogtleonardoexpress1leonardoexpress1jpg}
        tit="De Fiumicino a Roma."
        tags="#tips #trenes"
        fecha="7 de marzo de 2014"
      >
        ¡Ya estoy en el Aeropuerto! ¿Ahora, cómo llego a Roma? Opciones hay
        muchas pero la que más me gusta es tomar el tren para turista, el
        Leonardo Express.
        <a href="/tips-leonardo-express"> [Leer más...]</a>
      </Entrada>
      <Entrada
        boletos
        src={imgblogtconvalidareconvalida3jpg}
        tit="Timbrado de boletos de trenes."
        tags="#tips #trenes"
        fecha="7 de marzo de 2014"
      >
        ¿Cuándo y dónde timbrar los boletos de los trenes? Te presento las
        famosas maquinitas para «convalidare» los boletos de los trenes. Uso
        la palabra en italiano porque es como la vas a ver escrita en los
        boletos y en las máquinas. Las dos primeras fotos corresponden a la
        estación de trenes del aeropuerto de Fiumicino, localidad cercana a
        Roma (como nuestra Ezeiza). En realidad el aeropuerto se llama
        Leonardo da Vinci.
        <a href="/tips-convalidare"> [Leer más...]</a>
      </Entrada>
      <Entrada
        montalbano
        src={imgblogasiciliacasamontalbano01casamontalbano1jpg}
        tit="Fan de Montalbano."
        tags="#relatos #sicilia"
        fecha="7 de marzo de 2014"
      >
        En Punta Secca. Debo confesar que me encuentro entre los millones de
        fans de la serie de la RAI "Il commissario Montalbano" Así que,
        encontrándome en Sicilia, cómo no me iba a dar una vueltita por los
        lugares donde se la filma ¡Qué emoción, sentí al llegar a la casa de
        Montalbano! Se encuentra en Punta Secca, que en la serie se llama
        Marinella ¡La casa en la playa! ¡Qué linda que es!
        <a href="/anecdotas-casa-montalbano"> [Leer más...]</a>
      </Entrada>
      <Entrada
        balancita
        src={imgblogtbalancita1balancitajpg}
        tit="Balancita para valijas."
        tags="#tips #aviones #valijas"
        fecha="7 de marzo de 2014"
      >
        ¡Qué maravilla! Ya no tenés que sufrir más en los aeropuertos
        pensando cuánto pesará tu valija, ni tendrás que dejar cosas o,
        pagar ¡150 euros por unos kilos de más! Esta balancita es
        sencillísima. Enganchas la valija, sostenés la balancita, de la que
        cuelga la valija, apretás el botoncito rojo y esperás unos instantes
        que se estabilice la medida.
        <a href="/tips-balancita"> [Leer más...]</a>
      </Entrada>
      <Entrada
        celular
        src={imgblogtcelularcelularjpg}
        tit="¿Cómo me comunico?"
        tags="#tips #anécdotas"
        fecha="7 de marzo de 2014"
      >
        Es un tema que cambia continuamente, de manera que te diría que
        antes de viajar contactes a tu empresa de celular y preguntes cómo
        puedes usarlo y sobre todo los costos. Pero yo te voy a contar mi
        experiencia.
        <a href="/tips-celular"> [Leer más...]</a>
      </Entrada>
      <Entrada
        cinta
        src={imgblogtvalijas1valijaconcinta4ruedasjpg}
        tit="¿Cuál es mi valija? Socorro!!!"
        tags="#tips #valijas #anécdotas"
        fecha="7 de marzo de 2014"
      >
        Te puede sorprender pero sucede. Sí, que entre tantas valijas que
        hay en la cinta transportadora haya varias sino iguales por lo menos
        muy parecidas a las tuyas. Hay una solución muy práctica. Atarles en
        la manija alguna cinta, cuanto más rara mejor.
        <a href="/tips-valijas-1"> [Leer más...]</a>
      </Entrada>
      <Entrada
        avion
        src={imgblogtaviones1enezeizajpg}
        tit="¿Qué me pongo para viajar?"
        tags="#tips #aviones #anécdotas"
        fecha="7 de marzo de 2014"
      >
        ¡Socorro! Acá pleno verano allá pleno invierno. Es un problema
        porque muchas veces pasamos de pleno invierno a pleno verano o
        alrevés ¡Una vez cambié 40° C en unas horas! ¡En Roma 38° C y en
        Ezeiza 2° C bajo cero! Yo en el avión siempre tengo un poco de frío,
        claro que es porque viajo del lado de la ventana y se nota la
        diferencia de temperatura, tengo frío en la parte del cuerpo que
        está contra la ventana.
        <a href="/tips-viajando-en-avion-1"> [Leer más...]</a>
      </Entrada>
      <Entrada
        auto
        src={imgblogtautoViajandoenautojpg}
        tit="Cómo disfrutar tu viaje en auto."
        tags="#tips #autos"
        fecha="7 de marzo de 2014"
      >
        Viajando en auto.Escuchando «Radio Italia» ¡Qué placer increíble
        manejar por Italia escuchando todo el tiempo música italiana! Esto
        es posible sintonizando «Radio Italia». Música sin interrupciones
        ¡Te lo quería contar!
        <a href="/tips-viajando-en-auto"> [Leer más...]</a>
      </Entrada>
      <Entrada
        valijas
        src={imgblogtvalijas2enfiumicinoconvalijasjpg}
        tit="¿Con cuántas valijas viajo?"
        tags="#tips #aviones #valijas #anécdotas"
        fecha="7 de marzo de 2014"
      >
        Cómo acostumbrarme mal me significó grandes sufrimientos. En mis
        primeros viajes iba a la casa de mi novio y él me iba a buscar en su
        auto al aeropuerto, así que llevaba dos enormes valijas de 25 kg,
        <a href="/tips-valijas-2"> [Leer más...]</a>
      </Entrada>
      <Entrada
        licencia
        src={imgblogtlicenciaLicenciadeconducirinternacionaljpg}
        tit="¡Licencia de conducir internacional!"
        tags="#tips #autos"
        fecha="7 de marzo de 2014"
      >
        ¿Necesito la licencia de conducir internacional en Italia? Sí. Si
        uno lee en las fuentes autorizadas, por ejemplo, consulado de
        Italia, empresas de alquiler de autos, se ve que sí se necesita. No
        obstante decidí hacer una prueba, cuando fui a alquilar el auto
        presenté sólo la licencia argentina y ¿qué pasó? me pidieron también
        la internacional. Lo cual era absolutamente lógico, porque la
        nuestra estaba sólo en español,
        <a href="/tips-licencia"> [Leer más...]</a>
      </Entrada>
      <Entrada
        bienvenidos
        src={imgblogrbienvenidos1encastelsantangelojpg}
        tit="¡Bienvenidos a mi sitio!"
        tags="#relatos #roma"
        fecha="7 de marzo de 2014"
      >
        <a href="/anecdotas-roma-1"> [Leer más...]</a>
      </Entrada>
    </Entradas>
  </Marco>

export default BlogVlog

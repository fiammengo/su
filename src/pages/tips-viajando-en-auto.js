import React from "react"
import Marco from "../components/marco"
import Container from "@material-ui/core/Container"
import Minipaginas from "../components/minipaginas"
import Minipagina from "../components/minipagina"
import FotoConLeyenda from "../components/foto-con-leyenda"

import TextoEnMinipagina from "../components/texto-en-minipagina"
import { Tips } from "../components/estilos/estilos-de-articulo"

import imgblogtautoViajandoenautojpg from "../img/blog/t-auto/Viajando-en-auto.jpg"

const ViajandoEnAuto = () => (
  <Marco>
    <Tips>
      <Container maxWidth="xl">
        <Minipaginas paddingSuperior={20}>
          <Minipagina>
            <FotoConLeyenda src={imgblogtautoViajandoenautojpg} />
          </Minipagina>
          <Minipagina>
            <TextoEnMinipagina>
              <h3>Cómo disfrutar tu viaje en auto.</h3>
              <p>
                Viajando en auto. ¡Qué placer increíble manejar por Italia escuchando todo el tiempo música
                italiana! Esto es posible sintonizando «Radio Italia». Música sin interrupciones ¡Te lo quería
                contar!
              </p>
            </TextoEnMinipagina>
          </Minipagina>
          <Minipagina>
            <TextoEnMinipagina>
              <h3>Escuchando «Radio Italia»</h3>
              <p>
                Y también contarte la música que me acompañó en el 2012 y que me sigue gustando muchísimo:
                «Arriverà» interpretada por Modà & Emma, en el Festival de San Remo de ese año. Te la paso
                para que la puedas escuchar con un clic.
              </p>
            </TextoEnMinipagina>
          </Minipagina>
          <Minipagina>
            <FotoConLeyenda leyenda="Arriverà">
              <iframe
                src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/67538178&amp;color=%23797979&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;show_teaser=true&amp;visual=true"
                width="100%"
                height="300"
                title="Arriverà"
                frameborder="no"
                scrolling="no"
              ></iframe>
            </FotoConLeyenda>
          </Minipagina>
        </Minipaginas>
      </Container>
    </Tips>
  </Marco>
)
export default ViajandoEnAuto

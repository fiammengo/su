import React from "react"
import Marco from "../components/marco"
import Container from "@material-ui/core/Container"
import Minipaginas from "../components/minipaginas"
import Minipagina from "../components/minipagina"
import FotoConLeyenda from "../components/foto-con-leyenda"

import TextoEnMinipagina from "../components/texto-en-minipagina"
import { Tips } from "../components/estilos/estilos-de-articulo"

import imgblogtconvalidareconvalida3jpg from "../img/blog/t-convalidare/convalida3.jpg"
import imgblogtconvalidareconvalida2jpg from "../img/blog/t-convalidare/convalida2.jpg"

const TipsConvalidare = () => (
  <Marco>
    <Tips>
      <Container maxWidth="xl">
        <Minipaginas paddingSuperior={20}>
          <Minipagina>
            <FotoConLeyenda src={imgblogtconvalidareconvalida3jpg} />
          </Minipagina>
          <Minipagina>
            <TextoEnMinipagina>
              <h3>Timbrado de boletos de trenes.</h3>
              <p>
                ¿Cuándo y dónde timbrar los boletos de los trenes? Te presento las famosas maquinitas para
                «convalidare» los boletos de los trenes. Uso la palabra en italiano porque es como la vas a
                ver escrita en los boletos y en las máquinas. Las dos primeras fotos corresponden a la
                estación de trenes del aeropuerto de Fiumicino, localidad cercana a Roma (como nuestra
                Ezeiza). En realidad el aeropuerto se llama Leonardo da Vinci. Tendrías que usar esta
                maquinita si fueras a tomar el tren «Regionale veloce» que va a la estación Roma Tiburtina.
                Pero lo más probable es que te dirijas a Roma Termini y solo uses la lectora de código QR.
                Guardá a mano el ticket porque cuando pasa el guarda te lo pide. ¿Cuándo debo timbrar el
                boleto? Cuando vayas de Termini al aeropuerto o vayas a tomar cualquiera tren cuyo boleto no
                tenga fija la fecha, la hora y probablemente el asiento tendrás que timbrarlo. Las maquinitas
                se encuentran adosadas a las columnas. Te muestro en la tercera foto una en Termini. Ojo, si
                tenés que cambiar de andén tendrías que timbrar el boleto antes porque te puede pasar, te lo
                cuento por experiencia propia, que llegues corriendo con la lengua afuera (¿por qué será que
                yo siempre estoy corriendo trenes?) y a punto de perder el tren busques la famosa maquinita y
                descubras que no la hay y que eso significa que debas bajar una escalera, correr por un largo
                pasillo, subir otra escalera, buscar la maquinita y volver corriendo a hacer el trayecto
                inverso, tratando de no rodar y todo ¡con la valija a cuestas! etiqueta
              </p>
            </TextoEnMinipagina>
          </Minipagina>
          <Minipagina>
            <TextoEnMinipagina>
              <h3>Solución de emergencia</h3>
              <p>
                Te paso la posta. Si tu caso es que el tren en cuestión ya está en el andén, no tenés tiempo
                de buscar la maquinita entonces, subís al primer vagón, que es donde está el guarda y le das
                el boleto para que te lo timbre. Si no hacés así, cuando te lo pida y lo vea sin timbrar te
                hará pagar ¡una multa de €200!
              </p>
            </TextoEnMinipagina>
          </Minipagina>
          <Minipagina>
            <FotoConLeyenda src={imgblogtconvalidareconvalida2jpg} />
          </Minipagina>
        </Minipaginas>
      </Container>
    </Tips>
  </Marco>
)
export default TipsConvalidare

import React from "react"
import Marco from "../components/marco"
import Container from "@material-ui/core/Container"
import Minipaginas from "../components/minipaginas"
import Minipagina from "../components/minipagina"
import Video from "../components/video"
import FotoConLeyenda from "../components/foto-con-leyenda"

import TextoEnMinipagina from "../components/texto-en-minipagina"
import { Anecdotas } from "../components/estilos/estilos-de-articulo"

import imgblogaovejasappia01viadiceciliametellajpg from "../img/blog/a-ovejas-appia/01-via-di-cecilia-metella.jpg"
import imgblogaovejasappia02ovejasviadiceciliametellajpg from "../img/blog/a-ovejas-appia/02-ovejas-via-di-cecilia-metella.jpg"
import imgblogaovejasappia03ovejasviadiceciliametella2jpg from "../img/blog/a-ovejas-appia/03-ovejas-via-di-cecilia-metella2.jpg"
import imgblogaovejasappia04ovejasviadiceciliametella3jpg from "../img/blog/a-ovejas-appia/04-ovejas-via-di-cecilia-metella3.jpg"
import imgblogaovejasappia05ovejasviadiceciliametella4jpg from "../img/blog/a-ovejas-appia/05-ovejas-via-di-cecilia-metella4.jpg"

const AnecdotasOvejas = () => (
  <Marco>
    <Anecdotas>
      <Container maxWidth="xl">
        <Minipaginas paddingSuperior={20}>
          <Minipagina>
            <FotoConLeyenda
              zoom
              src={imgblogaovejasappia01viadiceciliametellajpg}
              title="Via di Cecilia Metella. Appia Antica. Roma."
              leyenda="Via di Cecilia Metella. Appia Antica. Roma."
            />
          </Minipagina>
          <Minipagina>
            <TextoEnMinipagina>
              <h3>Sorpresa en Roma.</h3>
              <p>
                Te cuento la anécdota. Caminaba por esa encantadora callecita cerca de la via Appia Antica,
                sin vereda, tratando de que los autos no me llevaran por delante, cuando sentí un extraño
                ruido a mis espaldas, pero verdaderamente extraño, nunca antes escuchado por mí. Difícil de
                explicar ¿tal vez telas que se rozaban en gran cantidad?
              </p>
            </TextoEnMinipagina>
          </Minipagina>
          <Minipagina>
            <TextoEnMinipagina>
              <h3>Un ruido inusual.</h3>
              <p>
                Me di vuelta ¿qué podía ser ese inusual ruido? Vi cientos de ovejitas y en un instante me
                encontré rodeada por ellas. Se rozaban unas con otras ocasionando el «extraño» rumor. ¡En
                Roma! Se dirigían al mismo parque que yo, ja, ja, pero ellas iban mucho más rápido, me dejaron
                atrás… Las corrí desconsolada, sobre todo porque del apuro creí que estaba filmando pero me
                había equivocado. Qué rabia. Quería alcanzarlas para volverlas a filmar pero fue imposible,
                sólo las registré alejándose. Aquí ves a las encantadoras ovejitas.
              </p>
            </TextoEnMinipagina>
          </Minipagina>
          <Minipagina>
            <FotoConLeyenda
              title="Te cuento qué me pasó cuando caminaba despreocupadamente."
              leyenda="Te cuento qué me pasó cuando caminaba despreocupadamente."
            >
              <Video
                src="https://www.youtube.com/embed/XbGZZhsO5Ak"
                title="Te cuento qué me pasó cuando caminaba despreocupadamente."
              />
            </FotoConLeyenda>
          </Minipagina>
          <Minipagina>
            <FotoConLeyenda
              zoom
              src={imgblogaovejasappia02ovejasviadiceciliametellajpg}
              title="Ovejas en via di Cecilia Metella, Appia Antica. Roma."
              leyenda="Ovejas en via di Cecilia Metella, Appia Antica. Roma."
            />
          </Minipagina>
          <Minipagina>
            <FotoConLeyenda
              title="Ovejitas con su pastor entrando al Parco della Caffarella. Es parte del Parco Regionale"
              leyenda="Ovejitas con su pastor entrando al Parco della Caffarella. Es parte del Parco Regionale"
            >
              <Video
                src="https://www.youtube.com/embed/w4J8IcYHAlg"
                title="Ovejitas con su pastor entrando al Parco della Caffarella. Es parte del Parco Regionale"
              />
            </FotoConLeyenda>
          </Minipagina>
          <Minipagina>
            <FotoConLeyenda
              zoom
              src={imgblogaovejasappia03ovejasviadiceciliametella2jpg}
              title="Ovejas en via di Cecilia Metella, Appia Antica. Roma."
              leyenda="Ovejas en via di Cecilia Metella, Appia Antica. Roma."
            />
          </Minipagina>
          <Minipagina>
            <FotoConLeyenda
              zoom
              src={imgblogaovejasappia04ovejasviadiceciliametella3jpg}
              title="Ovejas en via di Cecilia Metella, Appia Antica. Roma."
              leyenda="Ovejas en via di Cecilia Metella, Appia Antica. Roma."
            />
          </Minipagina>
          <Minipagina>
            <FotoConLeyenda
              zoom
              src={imgblogaovejasappia05ovejasviadiceciliametella4jpg}
              title="Ovejas en via di Cecilia Metella, Appia Antica. Roma."
              leyenda="Ovejas en via di Cecilia Metella, Appia Antica. Roma."
            />
          </Minipagina>
          <Minipagina>
            <FotoConLeyenda
              title="Ovejitas con su pastor en el Parco della Caffarella. Yéndose rápidamente."
              leyenda="Ovejitas con su pastor en el Parco della Caffarella. Yéndose rápidamente."
            >
              <Video
                src="https://www.youtube.com/embed/w4J8IcYHAlg"
                title="Ovejitas con su pastor en el Parco della Caffarella. Yéndose rápidamente."
              />
            </FotoConLeyenda>
          </Minipagina>
        </Minipaginas>
      </Container>
    </Anecdotas>
  </Marco>
)
export default AnecdotasOvejas

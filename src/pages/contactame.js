import React from "react"
import Marco from "../components/marco"
import WorkingOnIt from "../components/WorkingOnIt"
export default function Contactame() {
  return (
    <div>
      <Marco>
        <WorkingOnIt tit="Contáctame">Acá va a venir el formulario para contacto.</WorkingOnIt>
      </Marco>
    </div>
  )
}

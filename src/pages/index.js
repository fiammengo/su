import React from "react"
import Marco from "../components/marco"
import leoncio from "./leoncito.jpg"
import { css, keyframes } from "@emotion/core"
import styled from "@emotion/styled"

const descripcionindividual = css`
  font-family: "Architects Daughter", cursive;
  font-size: 28px;
  color: white;
  text-shadow: 1px 0 black;
  font-weight: 200;
  text-align: right;
  position: absolute;
  top: 45.7rem;
  right: 2rem;
`
const romaitalia = css`
  font-family: "Architects Daughter", cursive;
  font-size: 35px;
  color: white;
  text-shadow: 1px 0 black;
  font-weight: 300;
  text-align: right;
  position: absolute;
  top: 41.7rem;
  right: 2rem;
`
const Hip = keyframes`
 from, 25%, 50%, to {
    transform: scale(1.1);
  }
 from, 50%, 99%, to {
    transform: scale(1);
  }
`

// const Hop = keyframes`
//  from, 20%, 53%, 80%, to {
//     transform: translate3d(0,0,0);
//   }

//   40%, 43% {
//     transform: translate3d(0, -30px, 0);
//   }

//   70% {
//     transform: translate3d(0, -15px, 0);
//   }

//   90% {
//     transform: translate3d(0,-4px,0);
//   }
// `;

// const Text = styled("div")`
//   animation: ${Hop} 1s linear infinite;
// `;

const Paralax = styled("div")`
  animation: ${Hip} 40s linear;
`

// const Component = styled("div")`
//   .text {
//     position: absolute;
//     bottom: 80px;
//     line-height: 1.4;
//     display: flex;
//     justify-content: center;
//     flex-direction: column;
//   }
//   .romaitalia {
//     font-size: 35px;
//     color: white;
//     text-shadow: 1px 0 black;
//     font-weight: 300;
//     text-align: right;
//     position: absolute;
//     top: 41.7rem;
//     right: 2rem;
//   }
//   .country {
//     font-size: 38px;
//     color: white;
//     font-weight: 1000;
//     display: flex;
//     justify-content: center;
//   }
// `;

export default function Home() {
  return (
    <div>
      <Marco>
        <div className="baseimagen">
          <Paralax>
            <img id="leoncito" className="leoncito" src={leoncio} alt="" />
          </Paralax>
          <div className="imagenfondo text">
            <div css={romaitalia}>
              {/* <Text> */}
              <span>ROMA . ITALIA</span>
              {/* </Text> */}
            </div>
            <div css={descripcionindividual}>
              Anécdotas, tips. Recorriendo ... Viajando ... De todo un poco. Asesoramiento
            </div>
          </div>
        </div>
      </Marco>
    </div>
  )
}

import React from "react"
import Marco from "../components/marco"
import Container from "@material-ui/core/Container"
import Minipaginas from "../components/minipaginas"
import Minipagina from "../components/minipagina"
import FotoConLeyenda from "../components/foto-con-leyenda"

import TextoEnMinipagina from "../components/texto-en-minipagina"
import { Tips } from "../components/estilos/estilos-de-articulo"

import imgblogtbalancita1balancitajpg from "../img/blog/t-balancita/1-balancita.jpg"
import imgblogtbalancita2balancitajpg from "../img/blog/t-balancita/2-balancita.jpg"

const TipsBalancita = () => (
  <Marco>
    <Tips>
      <Container maxWidth="xl">
        <Minipaginas paddingSuperior={20}>
          <Minipagina>
            <FotoConLeyenda src={imgblogtbalancita1balancitajpg} />
          </Minipagina>
          <Minipagina>
            <TextoEnMinipagina>
              <h3>Balancita para valijas.</h3>
              <p>
                ¡Qué maravilla! Ya no tenés que sufrir más en los aeropuertos pensando cuánto pesará tu
                valija, ni tendrás que dejar cosas o, pagar ¡150 euros por unos kilos de más! Esta balancita
                es sencillísima. Enganchas la valija, sostienes la balancita, de la que cuelga la valija,
                aprietas el botoncito rojo y esperas unos instantes que se estabilice la medida
              </p>
              <p>
                ¿No es increíble? Esto te permite distribuir tu equipaje de manera de no pasarte en el peso.
                Pero...no todo resulta genial!!! Está muy buena para la valija de mano, la de 8 kg.
              </p>
              <p>
                Para las otras, las de 23 kg o de 25 kg, uhm!!! depende de la persona. A mí me resultó
                imposible sostener la balancita y la valija por tan solo unos minutos en esa posición,
                sencillamente, no pude. Hay que tener fuerza para hacerlo. Además es una posición muy
                incómoda. Lástima que no se pueda colgar de algo.
              </p>
            </TextoEnMinipagina>
          </Minipagina>
          <Minipagina>
            <TextoEnMinipagina>
              <h3>Balancita en acción.</h3>
              <p>
                Balancita portátil para valijas en acción. En este momento la valija "carry-on", la de hasta 8
                kg que puede ir en la cabina contigo, está colgando de la balancita y como puedes ver pesa 6,5
                kg, estoy salvada, ja, ja.
              </p>
            </TextoEnMinipagina>
          </Minipagina>
          <Minipagina>
            <FotoConLeyenda src={imgblogtbalancita2balancitajpg} />
          </Minipagina>
        </Minipaginas>
      </Container>
    </Tips>
  </Marco>
)
export default TipsBalancita

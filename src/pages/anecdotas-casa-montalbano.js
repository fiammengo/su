import React from "react"
import Marco from "../components/marco"
import Container from "@material-ui/core/Container"
import Minipaginas from "../components/minipaginas"
import Minipagina from "../components/minipagina"
import FotoConLeyenda from "../components/foto-con-leyenda"

import TextoEnMinipagina from "../components/texto-en-minipagina"
import { Relatos } from "../components/estilos/estilos-de-articulo"

import imgblogasiciliacasamontalbano01casamontalbano1jpg from "../img/blog/a-sicilia/casa-montalbano/01-casa-montalbano1.jpg"
import imgblogasiciliacasamontalbano02puntaseccapandajpg from "../img/blog/a-sicilia/casa-montalbano/02-punta-secca-panda.jpg"
import imgblogasiciliacasamontalbano03casamontalbano3jpg from "../img/blog/a-sicilia/casa-montalbano/03-casa-montalbano3.jpg"
import imgblogasiciliacasamontalbano04torrepuntaseccajpg from "../img/blog/a-sicilia/casa-montalbano/04-torre-punta-secca.jpg"
import imgblogasiciliacasamontalbano05vistaplayadesdecasamontalbanojpg from "../img/blog/a-sicilia/casa-montalbano/05-vista-playa-desde-casa-montalbano.jpg"
import imgblogasiciliacasamontalbano06estacionamientocasamontalbanojpg from "../img/blog/a-sicilia/casa-montalbano/06-estacionamiento-casa-montalbano.jpg"
import imgblogasiciliacasamontalbano07casamontalbano2jpg from "../img/blog/a-sicilia/casa-montalbano/07-casa-montalbano2.jpg"
import imgblogasiciliacasamontalbano08detallepuertacasamontalbanojpg from "../img/blog/a-sicilia/casa-montalbano/08-detalle-puerta-casa-montalbano.jpg"
import imgblogasiciliacasamontalbano09terrazassobrelaplayacasamontalbanojpg from "../img/blog/a-sicilia/casa-montalbano/09-terrazas-sobre-la-playa-casa-montalbano.jpg"
import imgblogasiciliacasamontalbano10casamontalbanoplayatorrejpg from "../img/blog/a-sicilia/casa-montalbano/10-casa-montalbano-playa-torre.jpg"
import imgblogasiciliacasamontalbano11casamontalbanodesdelaplayapuntaseccajpg from "../img/blog/a-sicilia/casa-montalbano/11-casa-montalbano-desde-la-playa-punta-secca.jpg"
import imgblogasiciliacasamontalbano12faropuntaseccajpg from "../img/blog/a-sicilia/casa-montalbano/12-faro-punta-secca.jpg"
import imgblogasiciliacasamontalbano13madonnafaropuntaseccajpg from "../img/blog/a-sicilia/casa-montalbano/13-madonna-faro-punta-secca.jpg"
import imgblogasiciliacasamontalbano14puntaseccafarojpg from "../img/blog/a-sicilia/casa-montalbano/14-punta-secca-faro.jpg"
import imgblogasiciliacasamontalbano15Yoconlacasademontalbanoalfondojpg from "../img/blog/a-sicilia/casa-montalbano/15-Yo-con-la-casa-de-montalbano-al-fondo.jpg"
import imgblogasiciliacasamontalbano16vientocasamontalbanobbbjpg from "../img/blog/a-sicilia/casa-montalbano/16-viento-casa-montalbanobbb.jpg"
import imgblogasiciliacasamontalbano17marinapuntaseccajpg from "../img/blog/a-sicilia/casa-montalbano/17-marina-punta-secca.jpg"
import imgblogasiciliacasamontalbano18vistadelaplayacasamontalbanojpg from "../img/blog/a-sicilia/casa-montalbano/18-vista-de-la-playa-casa-montalbano.jpg"

const AnecdotasCasaMontalbano = () => (
  <Marco>
    <Relatos>
      <Container maxWidth="xl">
        <Minipaginas paddingSuperior={20}>
          <Minipagina>
            <FotoConLeyenda
              zoom
              src={imgblogasiciliacasamontalbano01casamontalbano1jpg}
              title="La casa de Il Commissario Montalbano. Punta Secca. Sicilia."
              leyenda="La casa de 'Il Commissario Montalbano'. Punta Secca. Sicilia."
            />
          </Minipagina>
          <Minipagina>
            <TextoEnMinipagina>
              <h3>Fan de Montalbano.</h3>
              <p>
                Debo confesar que me encuentro entre los millones de fans de la serie de la RAI "Il
                commissario Montalbano".
              </p>
              <p>
                Así que, encontrándome en Sicilia, cómo no me iba a dar una vueltita por los lugares donde se
                la filma ¡Qué emoción sentí al llegar a la casa de Montalbano! Se encuentra en Punta Secca,
                que en la serie se llama Marinella ¡La casa en la playa! ¡Qué linda que es!
              </p>
              <p>
                Parece ser que la casa funciona como bed and breakfast, aunque no tiene ninguna indicación,
                por lo menos a la vista, de este otro uso. Lo que sé, es que está muy bien mantenida.
              </p>
              <p>
                Fue un larguísimo viaje… Anduve perdida en el auto y dando vueltas en redondo por horas. El
                hecho de que yo siempre ande perdida manejando será tema de otros relatos!!!
              </p>
              <p>
                Literalmente perdí horas ¡Llegué cuando atardecía! Así que me quedé sin ir a Ragusa, que en la
                «RAI fiction» se llama Montelusa ¡Tengo que volver!
              </p>
              <p>
                Porqué será que siempre que conozco un lugar en Italia termino diciéndome "¡tengo que volver!"
              </p>
            </TextoEnMinipagina>
          </Minipagina>
          <Minipagina>
            <TextoEnMinipagina>
              <h3>Llegando a Punta Secca en el auto.</h3>
              <p>
                Estacioné el auto exhausta. Bajo. Veo unas señoras y les pregunto por la casa de Montalbano y
                me dicen que estaba justo allí, en la esquina, a unos metros y … la ví!!!
              </p>
              <p>
                Así que a caminar esos pocos metros que me separaban de mi objetivo. Y lo primero que vi es
                una entrada a la casa que no se ve en la serie. Se accede por una escalera externa.
              </p>
              <p>
                En la foto se ve en primer lugar la escalera, que mencioné, que pertenece a la casa de «El
                Commissario Montalbano». Y al fondo un quiosco, detrás del cuál estaba estacionado mi auto ¿Lo
                alcanzas a ver? Un Fiat Panda blanco.
              </p>
              En la siguiente foto te muestro, desde un ángulo desconocido, la casa iluminada, era casi de
              noche.
              <p>
                Por donde se la mire es hermosa. Da gusto ver que está impecable. Di la vuelta entorno para
                verla desde los tres lados libres.
              </p>
            </TextoEnMinipagina>
          </Minipagina>
          <Minipagina>
            <FotoConLeyenda
              zoom
              src={imgblogasiciliacasamontalbano02puntaseccapandajpg}
              title="Mi Fiat Panda estacionado detrás del kiosco. Punta Secca. Sicilia."
              leyenda="Mi Fiat Panda estacionado detrás del kiosco. Punta Secca. Sicilia."
            />
          </Minipagina>
          <Minipagina>
            <FotoConLeyenda
              zoom
              src={imgblogasiciliacasamontalbano03casamontalbano3jpg}
              title="La casa de Il Commissario Montalbano. Punta Secca. Sicilia."
              leyenda="La casa de Il Commissario Montalbano. Punta Secca. Sicilia."
            />
          </Minipagina>
          <Minipagina>
            <TextoEnMinipagina>
              <h3>Recorriendo los alrededores.</h3>
              <p>
                Puedes apreciar la entrada en lo alto de la escalera y el mar crecido llegando a la terraza
                del frente.
              </p>
              <p> ¿Cómo son los alrededores de la casa? </p>
              <p>
                Del otro lado del «estacionamiento donde el comisario deja su auto» (que no es tal en la
                realidad), hay una torre y en la base una confitería con mesitas en una terraza sobre el mar.
                Puedes verla en la siguiente foto.
              </p>
              <p>
                Recorrí todos los alrededores de la casa para conocer algo de Punta Secca, que me resultó un
                lugar encantador. Pequeño y muy prolijo.
              </p>
              <p>
                También vi el faro, el que se muestra en la vista aérea al principio de la serie. Y la pequeña
                marina.
              </p>
            </TextoEnMinipagina>
          </Minipagina>
          <Minipagina>
            <TextoEnMinipagina>
              <h3>Título</h3>
              <p>
                Agregué más fotos para que puedas darte una idea de cómo es este encantador pueblito y también
                para que puedas ver detalles de la casa y vistas desde otros ángulos. Espero que no te parezca
                que soy demasiado fanática ¿o lo seré realmente?
              </p>
              <p>
                Hay vistas de la casa desde la playa, ya que di la vuelta alrededor ¡Tanto me gustó! También
                fotos de la marina, del faro, en fin, fotos como para puedas imaginarte la casa en su
                contexto.
              </p>
              <p> Pero… ¿Qué ocurre? </p>
              <p>
                ¡Se encienden las luces! Cae el sol… ¡Y yo que tenía que seguir manejando hasta mi próximo
                destino y no quería llegar de noche! Ni un café pude tomar en este lugar. A seguir viaje y a
                rogar que no me pierda… ¿Crees que lo logré?
              </p>
              <p>
                Te adelanto el final. Fue inútil. Me perdí. Llegué de noche y tan tarde que me quedé sin
                cenar!!!! Será motivo de otro relato.
              </p>
              <p> Sucedió un 25 de marzo de 2017. </p>
            </TextoEnMinipagina>
          </Minipagina>
          <Minipagina>
            <FotoConLeyenda
              zoom
              src={imgblogasiciliacasamontalbano04torrepuntaseccajpg}
              title="Torre enfrente de la casa de Montalbano. Punta Secca. Sicilia."
              leyenda="Torre enfrente de la casa de Montalbano. Punta Secca. Sicilia."
            />
          </Minipagina>
          <Minipagina>
            <FotoConLeyenda
              zoom
              src={imgblogasiciliacasamontalbano05vistaplayadesdecasamontalbanojpg}
              title="Vista de la playa desde la casa de Montalbano. Punta Secca. Sicilia."
              leyenda="Vista de la playa desde la casa de Montalbano. Punta Secca. Sicilia."
            />
          </Minipagina>
          <Minipagina>
            <FotoConLeyenda
              zoom
              src={imgblogasiciliacasamontalbano06estacionamientocasamontalbanojpg}
              title="Estacionamiento de la casa de Montalbano. Punta Secca. Sicilia."
              leyenda="Estacionamiento de la casa de Montalbano. Punta Secca. Sicilia."
            />
          </Minipagina>
          <Minipagina>
            <FotoConLeyenda
              zoom
              src={imgblogasiciliacasamontalbano07casamontalbano2jpg}
              title="La casa de Il Commissario Montalbano. Punta Secca. Sicilia."
              leyenda="La casa de 'Il Commissario Montalbano'. Punta Secca. Sicilia."
            />
          </Minipagina>
          <Minipagina>
            <FotoConLeyenda
              zoom
              src={imgblogasiciliacasamontalbano08detallepuertacasamontalbanojpg}
              title="Puerta de entrada a la casa de Montalbano. Punta Secca. Sicilia."
              leyenda="Puerta de entrada a la casa de Montalbano. Punta Secca. Sicilia."
            />
          </Minipagina>
          <Minipagina>
            <FotoConLeyenda
              zoom
              src={imgblogasiciliacasamontalbano09terrazassobrelaplayacasamontalbanojpg}
              title="Terraza sobre la playa. Casa de Montalbano. Punta Secca. Sicilia."
              leyenda="Terraza sobre la playa. Casa de Montalbano. Punta Secca. Sicilia."
            />
          </Minipagina>
          <Minipagina>
            <FotoConLeyenda
              zoom
              src={imgblogasiciliacasamontalbano10casamontalbanoplayatorrejpg}
              title="La casa de Montalbano. Playa y torre. Punta Secca. Sicilia."
              leyenda="La casa de Montalbano. Playa y torre. Punta Secca. Sicilia."
            />
          </Minipagina>
          <Minipagina>
            <FotoConLeyenda
              zoom
              src={imgblogasiciliacasamontalbano11casamontalbanodesdelaplayapuntaseccajpg}
              title="Casa de Montalbano desde la playa. Punta Secca. Sicilia."
              leyenda="Casa de Montalbano desde la playa. Punta Secca. Sicilia."
            />
          </Minipagina>
          <Minipagina>
            <FotoConLeyenda
              zoom
              src={imgblogasiciliacasamontalbano12faropuntaseccajpg}
              title="Faro. Punta Secca. Sicilia."
              leyenda="Faro. Punta Secca. Sicilia."
            />
          </Minipagina>
          <Minipagina>
            <FotoConLeyenda
              zoom
              src={imgblogasiciliacasamontalbano13madonnafaropuntaseccajpg}
              title="Imagen de la Madonna y atrás el faro. Punta Secca. Sicilia."
              leyenda="Imagen de la Madonna y atrás el faro. Punta Secca. Sicilia."
            />
          </Minipagina>
          <Minipagina>
            <FotoConLeyenda
              zoom
              src={imgblogasiciliacasamontalbano14puntaseccafarojpg}
              title="Faro. Punta Secca. Sicilia."
              leyenda="Faro. Punta Secca. Sicilia."
            />
          </Minipagina>
          <Minipagina>
            <FotoConLeyenda
              zoom
              src={imgblogasiciliacasamontalbano15Yoconlacasademontalbanoalfondojpg}
              title="Aquí estoy, en la playa y la casa de Montalbano al fondo. Punta Secca. Sicilia."
              leyenda="Aquí estoy, en la playa y la casa de Montalbano al fondo. Punta Secca. Sicilia."
            />
          </Minipagina>
          <Minipagina>
            <FotoConLeyenda
              zoom
              src={imgblogasiciliacasamontalbano16vientocasamontalbanobbbjpg}
              title="Aquí estoy, en la playa, con viento fuerte y la casa de Montalbano al fondo. Punta Secca. Sicilia."
              leyenda="En la playa, con viento fuerte. Casa de Montalbano al fondo. Punta Secca. Sicilia."
            />
          </Minipagina>
          <Minipagina>
            <FotoConLeyenda
              zoom
              src={imgblogasiciliacasamontalbano17marinapuntaseccajpg}
              title="Marina. Punta Secca. Sicilia."
              leyenda="Marina. Punta Secca. Sicilia."
            />
          </Minipagina>
          <Minipagina>
            <FotoConLeyenda
              zoom
              src={imgblogasiciliacasamontalbano18vistadelaplayacasamontalbanojpg}
              title="Vista de la playa desde la casa de Montalbano. Punta Secca. Sicilia."
              leyenda="Vista de la playa desde la casa de Montalbano. Punta Secca. Sicilia."
            />
          </Minipagina>
        </Minipaginas>
      </Container>
    </Relatos>
  </Marco>
)
export default AnecdotasCasaMontalbano

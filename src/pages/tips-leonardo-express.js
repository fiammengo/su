import React from "react"
import Marco from "../components/marco"
import Container from "@material-ui/core/Container"
import Minipaginas from "../components/minipaginas"
import Minipagina from "../components/minipagina"
import FotoConLeyenda from "../components/foto-con-leyenda"

import TextoEnMinipagina from "../components/texto-en-minipagina"
import { Tips } from "../components/estilos/estilos-de-articulo"

import imgblogtleonardoexpress1leonardoexpress1jpg from "../img/blog/t-leonardo-express/1-leonardo-express-1.jpg"
import imgblogtleonardoexpressValijaseneltrenjpg from "../img/blog//t-leonardo-express/Valijas-en-el-tren.jpg"

const TipsLeonardoExpress = () => (
  <Marco>
    <Tips>
      <Container maxWidth="xl">
        <Minipaginas paddingSuperior={20}>
          <Minipagina>
            <FotoConLeyenda src={imgblogtleonardoexpress1leonardoexpress1jpg} />
          </Minipagina>
          <Minipagina>
            <TextoEnMinipagina>
              <h3>De Fiumicino a Roma.</h3>
              <p>
                ¡Ya estoy en el Aeropuerto! ¿Ahora, cómo llego a Roma? Opciones hay muchas pero la que más me
                gusta es tomar el tren para turista, el Leonardo Express.
              </p>
              <h3>¿Qué es el Leonardo express? </h3>
              <p>
                Es un tren directo desde el aeropuerto Leonardo da Vinci (pero que todos conocemos por
                Fiumicino por el nombre de la localidad donde se encuentra) hasta Termini, la estación
                terminal de trenes de Roma
              </p>
              <p> ¡En 32 minutos estás en el centro de Roma! </p>
              <p>
                Allí pasan 2 líneas de subtes y además hay una terminal de ómnibus y, por supuesto, parada de
                taxis.
              </p>
              <p>
                No necesitás reservar. Tal vez para hora pico convenga comprar el pasaje online. En los
                horarios en que llegué nunca tuve problemas.
              </p>
            </TextoEnMinipagina>
          </Minipagina>
          <Minipagina>
            <TextoEnMinipagina>
              <h3>¿Cómo lo encuentro?</h3>
              <p>
                En el aeropuerto sigo las indicaciones (cartel amarillo con el dibujo del tren) y llego sin
                problemas a la estación de trenes. En algunas partes hay cintas transportadoras en el piso
                para ir más rápido y con comodidad. No te sorprendas si tenés que bajar escaleras mecánicas,
                caminar, y volver a subir escaleras mecánicas. También podés buscar ascensores. Es que la
                estación, naturalmente está en otro edificio.
              </p>
            </TextoEnMinipagina>
          </Minipagina>
          <Minipagina>
            <FotoConLeyenda src={imgblogtleonardoexpressValijaseneltrenjpg} />
          </Minipagina>
        </Minipaginas>
      </Container>
    </Tips>
  </Marco>
)
export default TipsLeonardoExpress

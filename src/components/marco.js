import React from 'react';
import { ThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Encabezamiento from './encabezamiento'
import Pie from './pie'

const susanaTheme = createMuiTheme({
  palette: {
    primary: {
      main: '#16455a',
    },
    secondary: {
      main: '#f44336',
    },
  },
});

const Marco = ({ children }) => 
<ThemeProvider theme={susanaTheme}>
  <Encabezamiento />
    {children}
  <Pie />
</ThemeProvider>
export default Marco

import React from "react"

const Video=({src, title})=>(
<div className="video-wrapper">
<iframe
  className="video"
  src={src}
  allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
  allowFullScreen
  title={title} 
></iframe>
</div>
)

export default Video
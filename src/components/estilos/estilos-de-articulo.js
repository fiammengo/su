import styled from '@emotion/styled'

export const Relatos = styled.div`
  background-color: #74a7be;
  color: white;
`
export const Anecdotas = styled.div`
  background-color: #d9ea80;
  color: black;
`
export const Tips = styled.div`
  background-color: #bd0051;
  color: white;
`
/*71e35d*/
export const TipsVerde = styled.div`
  background-color: #d7ecc5;
  color: black;
`


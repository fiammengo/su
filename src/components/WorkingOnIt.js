import React from 'react'
import Typography from '@material-ui/core/Typography'
import woi from './underconstruction.jpeg'
const WorkingOnIt = props => <div style={{ margin: '100px auto', textAlign: 'center', color: '#16455a' }}>
  <Typography variant='h1'>{props.tit}</Typography>  
  <div style={{ backgroundColor: 'transparent', borderRadius: 12, padding: 48, width: 200, margin: '0 auto' }}>
    <img src={woi} alt="" style={{borderRadius: '50% 50%'}} className="spreaded"/>
  </div>
  <p>{props.children}</p>
</div>

export default WorkingOnIt

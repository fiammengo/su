import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faYoutube, faFacebook, faInstagram } from '@fortawesome/free-brands-svg-icons'
import { Link } from "gatsby"
import SubrayadoCuandoPasanPorAhi from './estilos/subrayado-cuando-pasan-por-ahi'

const Pie = () => <div className="pie" style={{ paddingLeft: '24px', paddingRight: '24px', }}>
  <div className="telefono">
    <div className="telefonotelefono">
      ☎ (011) 4543-5278 <br />
            info@susanacanel.com <br />
      <a href="https://www.youtube.com/susanacanel" className="redes-sociales">
        <span>
          <FontAwesomeIcon icon={faYoutube}  className="fa-iconito" />
          YouTube
        </span>
      </a><br />
      <a href="https://www.facebook.com/romatoursusy/" className="redes-sociales">
        <span>
          <FontAwesomeIcon icon={faFacebook}  className="fa-iconito-facebook"/>
          Facebook
        </span>
      </a><br />
      <a href="https://instagram.com/canelsusana/" className="redes-sociales">
        <span>
          <FontAwesomeIcon icon={faInstagram} className="fa-iconito-instagram"/>
          Instagram
        </span>
      </a><br />
    </div>
  </div>
  <div className="politica">
    <div className="politicapolitica">
      <span style={{paddingRight: 3}}>Copyright ©</span>
      <Link to="/sobremi/" activeClassName="perfil" className="perfil" >
        <SubrayadoCuandoPasanPorAhi>
          Susana Canel
        </SubrayadoCuandoPasanPorAhi>
      </Link>
      <br /> 
      <Link to="/politica/" activeClassName="perfil" className="perfil" >
        <SubrayadoCuandoPasanPorAhi>
          Política de privacidad
        </SubrayadoCuandoPasanPorAhi>
      </Link>
      <br/>
      <span className="autora">Sitio creado por: Susana Canel</span>
    </div>
  </div>
  <div className="direccion">
    <div className="direcciondireccion">
      Belgrano <br />Buenos Aires <br />Argentina
          </div>
  </div>
</div >

export default Pie

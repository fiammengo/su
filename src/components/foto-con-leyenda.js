import React from "react"
import { css } from "@emotion/core"

const foto = css`
  width: 93.8%;
  padding: 16px;
  border: 1px solid #444444;
  margin-top: 4px;
  margin-bottom: 4px;
  margin-left: 12px;
  border-radius: 8px;
  padding-left: 16px;
  background-color: #ffffff;
  padding-bottom: 2px;
  @media (max-width: 900px) {
      width: 100%;
  }
  @media (max-width: 500px) {
    width: 100%;
    padding-left: 8px;
    padding-right: 8px;
  }
  `
const foto_interior = css`
  width: 100%;
  border-radius: 6px;
`
const zooming = css`
  transition: 0.9s;
  &:hover {
    transform: scale(1.3);
  }
`
//className={"foto-interior" + zoom ? " zoom" : ""}

const FotoConLeyenda = ({ zoom, src, title, leyenda, children }) => (
  <div css={foto}>
    {
      src ?  <img 
              css={zoom ? [foto_interior, zooming] : foto_interior} 
              src={src} 
              alt=""
              title={title} /* title={title ? title : ""} eso lo hace el React */ />
          : <div>
              {children}
            </div> 
    }
    <div className="leyenda">{leyenda} </div>
  </div>
)

export default FotoConLeyenda

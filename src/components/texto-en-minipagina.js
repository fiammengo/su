import React from "react"
import { css } from "@emotion/core"

const estiloTextoEnMinipagina = css`
  padding: 1.5rem 0rem 1.5rem 1.4rem;
`
const TextoEnMinipagina = ({ children }) => (
  <div css={estiloTextoEnMinipagina} className="textoEnMinipagina">
    {children}
  </div>
)

export default TextoEnMinipagina

import React from 'react'
import Grid from '@material-ui/core/Grid'
import { css }  from '@emotion/core'

const estilo = css`
 padding: 0 24px;
`
const palidez = css`
  background-color: #ffffff60;
  border-radius: 8px;
`

const Minipagina = ({ children, palida}) => {
  return <Grid sm={12} md={8} lg={6} item>
  <div css={palida ? [estilo, palidez] : estilo}>
    {children}
  </div>
</Grid >
}
export default Minipagina